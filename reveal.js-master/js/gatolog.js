
$(document).ready(function(e) {
  

  $("#fetch").click(function(){
    $(this).prop("disabled", true);
    $(this).html('<span class="spinner spinner-border spinner-border-sm float-right" role="status" aria-hidden="true"></span> Vibing...');
  });
  

  $("#cancel").click(function(e) {

    $(".spinner").removeClass();
    $("#fetch").html('Publish'); 
    $("#fetch").removeAttr("disabled");

    e.preventDefault();
    e.stopPropagation();
  })


  $("#close-modal").click(function(e){
    $(".spinner").removeClass();
    $("#fetch").html('Publish'); 
    $("#fetch").removeAttr("disabled");

    e.preventDefault();
    e.stopPropagation();
  });

  $("#slide-cart").click(function(){
    $(this).hide();
  })

  $('[class^=navigate]').click(function(e){
    e.preventDefault();
    e.stopPropagation();
  });




  $('button[data-bs-toggle="offcanvas"]').click(function(){  
    $('button[data-bs-toggle="offcanvas"]').hide(); 
  });



  $('button[data-bs-dismiss="offcanvas"]').click(function(){
    $('button[data-bs-toggle="offcanvas"]').show();
  })


      
  $('button[data-bs-toggle="modal"]').click(function(e){
    e.preventDefault();
    e.stopPropagation();

    $(".sortable#publish").empty();
    

    $("#commit li").clone().appendTo(".sortable#publish");
    
  });

 



  $(".offcanvas-body").click(function(e) {
    
    e.preventDefault();
    
    let $current_id = $('section.present').attr('data-slide-id');
    
    if ( $('li[id="' + $current_id + '"]').length){
      return false;
    } else {
      $(".sortable#commit").append('<div class="item-wrapper"><span style="float: left"><button type="button" class="btn btn-outline-danger btn-sm deleteItem">Delete</button>&nbsp; &nbsp; </span><li id=' + $current_id  + '>' + $current_id + '</li></div>');
    }
  });  


  $( '.sortable#commit' ).on("click", ".btn", function(e) {
    
    e.preventDefault()
    e.stopPropagation()
    
      //console.log("Detached");
      $(this).closest(".item-wrapper").detach();
  
  });




  $( function() {
    $( ".sortable" ).sortable({
      placeholder: "ui-state-highlight"
    });
    
    $( ".sortable" ).disableSelection();
  });
});
